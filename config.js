module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  baseBranches: ['master'],
  labels: ['renovate'],
  rebaseWhen: 'auto',
  automerge: true,
  automergeType: 'branch',
  semanticCommits: true,
  major: {
    automerge: false
  },
  repositories: [
    "pipct/renovate",
    "pipct/my-timetable"
  ],
  extends: [
    'config:base',
    ":pinAllExceptPeerDependencies"
  ]
};
